package com.sharknado.aplicacaografica;

import com.sharknado.aplicacaografica.AndGraph.AGGameManager;
import com.sharknado.aplicacaografica.AndGraph.AGInputManager;
import com.sharknado.aplicacaografica.AndGraph.AGScene;
import com.sharknado.aplicacaografica.AndGraph.AGScreenManager;
import com.sharknado.aplicacaografica.AndGraph.AGSprite;

public class CenaJogo extends AGScene {

    AGSprite gato;

    /*******************************************
     * Name: CAGScene()
     * Description: Scene construtor
     * Parameters: CAGameManager
     * Returns: none
     *****************************************
     * @param pManager*/
    public CenaJogo(AGGameManager pManager) {
        super(pManager);
    }

    @Override
    public void init() {
        //Chamada toda vez que a cena for ativada, exibida
        setSceneBackgroundColor(0,1,1);

        gato = createSprite(R.mipmap.sprites, 2, 4);
        gato.setScreenPercent(60, 30);
        gato.vrPosition.setXY(AGScreenManager.iScreenWidth/2, AGScreenManager.iScreenHeight/2);
        gato.addAnimation(7, true,0,7);
        gato.iMirror = AGSprite.HORIZONTAL;
        gato.fAlpha = 0.5f;
    }

    @Override
    public void restart() {
        //Chamado após o retorno de uma interrupção
    }

    @Override
    public void stop() {
        //Chamado quando uma interrupção acontecer
    }

    @Override
    public void loop() {
        //Chamado n vezes por segundo
        gato.fAngle += 0.5f;
        if (AGInputManager.vrTouchEvents.screenClicked()){
            vrGameManager.setCurrentScene(2);
        }
    }
}