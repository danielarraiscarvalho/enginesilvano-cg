package com.sharknado.aplicacaografica;

import com.sharknado.aplicacaografica.AndGraph.AGGameManager;
import com.sharknado.aplicacaografica.AndGraph.AGScene;
import com.sharknado.aplicacaografica.AndGraph.AGSoundEffect;
import com.sharknado.aplicacaografica.AndGraph.AGSoundManager;
import com.sharknado.aplicacaografica.AndGraph.AGTimer;

public class CenaSonora extends AGScene {

    int som;

    int musica;

    /*******************************************
     * Name: CAGScene()
     * Description: Scene construtor
     * Parameters: CAGameManager
     * Returns: none
     *****************************************
     * @param pManager*/
    public CenaSonora(AGGameManager pManager) {
        super(pManager);
        som = AGSoundManager.vrSoundEffects.loadSoundEffect("toc.wav");
        AGSoundManager.vrSoundEffects.play(som);

        AGSoundManager.vrMusic.loadMusic("musica.mp3", true);
        AGSoundManager.vrMusic.play();
    }

    @Override
    public void init() {
//Chamada toda vez que a cena for ativada, exibida
        setSceneBackgroundColor(1,1,0);

    }

    @Override
    public void restart() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void loop() {

    }
}
