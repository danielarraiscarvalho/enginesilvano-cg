package com.sharknado.aplicacaografica;

import android.os.Bundle;

import com.sharknado.aplicacaografica.AndGraph.AGActivityGame;

public class Principal extends AGActivityGame {

    CenaHome abertura = null;
    CenaConf creditos = null;
    CenaJogo home = null;
    CenaSonora som = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Inicializa o motor gráfico
        init(this, false);

        abertura = new CenaHome(getGameManager());
        home = new CenaJogo(getGameManager());
        creditos = new CenaConf(getGameManager());
        som = new CenaSonora(getGameManager());

        //Registra a cena no gerenciador
        getGameManager().addScene(abertura);
        getGameManager().addScene(home);
        getGameManager().addScene(creditos);
        getGameManager().addScene(som);
    }
}
